import React , { Component } from 'react';
import Order from '../../Components/Order/Order';
import Spinner from '../../Components/UI/Spinner/Spinner';
import axios from '../../axios';

class Orders extends Component {

    state = {
        orders : [],
        loading : true,
    }
    componentDidMount(){
        axios.get('/orders.json')
         .then(response => {
             const data = response.data;
             const orders = [];
             Object.keys(data).map(orderid => {
                 orders.push({ ...data[orderid] , id : orderid});
             });
             this.setState({orders : orders, loading : false});
             console.log(orders);
         })
          .catch(error => this.setState({loading : false}));
    }

    render(){

        let orders = <Spinner />
        if(!this.state.loading) {
            orders = (
                <div>
                     {this.state.orders.map(order => (
                         <Order
                           ingredients = {order.ingredients}
                           price = {order.price}
                           name = {order.name}
                           key={order.id}
                           />
                     ))}
                </div>
            )
        }
        return (
            <div>
                {orders}
            </div>
        );
    };
};


export default Orders;