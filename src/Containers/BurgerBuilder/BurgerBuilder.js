import React , {Component} from 'react';

import Burger from '../../Components/Burger/Burger';
import BuildControls from '../../Components/Burger/BuildControls/BuildControls';
import OrderSummary from '../../Components/Burger/OrderSummary/OrderSummary';
import Modal from '../../Components/UI/Modal/Modal';
import Aux from '../../hoc/Auxiliary';
import { connect } from 'react-redux';
import Spinner from '../../Components/UI/Spinner/Spinner';
import * as actionTypes from '../../Store/actions';

class BurgerBuilder extends Component {

    state = {
        purchasable : false,
        purchasing : false,
        loading : false,
    };
    onSuccessClicked= () =>{
        this.props.history.push({
            pathname : '/checkout',
        });
    }
    onOrderNowClicked = () =>{
        this.setState({purchasing : true});
    }
    onModalCancel = () =>{
        this.setState({purchasing : false});
    }
    checkPurchasable = (ingredients) => {
        let isPurchasable = false;
        Object.keys(ingredients).map( ig =>{
            if(ingredients[ig] > 0)
            isPurchasable =true;
        });
        return isPurchasable;
    }
    render(){
        let disabledInfo = {
            ...this.props.ingredients,
        };
        for(let key in disabledInfo){
            disabledInfo[key] = disabledInfo[key] ===0;
        }
        let orderSummary = <OrderSummary 
          ingredients={this.props.ingredients} 
          purchaseCancel={this.onModalCancel}
          purchaseSuccess={this.onSuccessClicked}
          billingAmount = {this.props.totalPrice}
        />
        let burger  = <Spinner />
        if(Object.keys(this.props.ingredients).length > 0){
            burger = <Burger ingredients={this.props.ingredients} /> ;
        }
        if(this.state.loading) {
            orderSummary = <Spinner />
        }
        return(
            <Aux>
                <Modal show={this.state.purchasing} onModalCancel ={this.onModalCancel}>
                    {orderSummary}
                </Modal>
                {burger}
                <BuildControls 
                  addIngredient={(ingredient) => this.props.addIngredient(ingredient)}
                  removeIngredient={(ingredient) => this.props.removeIngredient(ingredient)}
                  disabledInfo={disabledInfo}
                  price = {this.props.totalPrice}
                  onOrder={this.onOrderNowClicked}
                  orderButtonDisabled ={!this.checkPurchasable(this.props.ingredients)}
                />
            </Aux>
        )
    }
};

const mapStateToProps = (state) => {
     return {
         ingredients : state.ingredients,
         totalPrice : state.totalPrice,
     }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addIngredient : (ingredient) => dispatch({type : actionTypes.ADD_INGREDIENT, ingredient : ingredient}),
        removeIngredient : (ingredient) => dispatch({type : actionTypes.REMOVE_INGREDIENT, ingredient : ingredient}),
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(BurgerBuilder);