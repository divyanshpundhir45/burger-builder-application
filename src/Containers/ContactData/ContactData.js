import React , { Component } from 'react';
import Button from '../../Components/UI/Button/Button';
import styles from './ContactData.module.css';
import axios from '../../axios';
import Spinner from '../../Components/UI/Spinner/Spinner';
import Input from '../../Components/UI/Input/Input';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class ContactData extends Component {
    state = {
        Orderform : {
            name : {
                elementType : 'input',
                elementConfig : {
                    type : 'text',
                    placeholder : 'Your Name'
                },
                value : '', 
                isValid : false,
                validation : {
                    isRequired : true,
                },
                touched : false,
            },
            email : {
                elementType : 'input',
                elementConfig : {
                    type : 'email',
                    placeholder : 'Your Email'
                },
                value : '',
                isValid : false,
                validation : {
                    isRequired : true,
                },
                touched : false,
            },
            zipCode : {
                elementType : 'input',
                elementConfig : {
                    type : 'text',
                    placeholder : 'Your ZipCode',
                },
                value : '',
                isValid : false,
                validation : {
                    isRequired : true,
                },
                touched : false,
            },
            country : {
                elementType : 'input',
                elementConfig : {
                    type : 'text',
                    placeholder : 'Your Country',
                },
                value : '',
                isValid : false,
                validation : {
                    isRequired : true,
                },
                touched : false,
            },
            street : {
                elementType : 'input',
                elementConfig : {
                    type : 'text',
                    placeholder : 'Your Street address',
                },
                value : '',
                isValid : false,
                validation : {
                    isRequired : true,
                },
                touched : false,
            },
            deliveryMethod : {
                elementType : 'select',
                elementConfig : {
                    options : [
                    {
                        value : 'fastest',
                        displayValue : 'Fastest'
                    },
                    {
                        value : 'cheapest',
                        displayValue : 'Cheapest',
                    }]
                },
                value : 'fastest',
                isValid : true,
                validation : {
                    isRequired : true,
                },
                shouldNotValidate : true,
                touched : false,
            },
        },
        loading : false,
        formIsValid : false,
    };

    onOrderClicked = (event) =>{
       event.preventDefault();
       this.setState({loading : true});
       let formdata = {};
       Object.keys(this.state.Orderform).map(ele => {
           formdata[ele] = this.state.Orderform[ele].value;
       })
       const order = {
           ingredients : this.props.ingredients,
           price : this.props.price,
           ...formdata,
       };
       console.log(order);
       axios.post('/orders.json',order)
        .then(response => {
            console.log(response);
            this.setState({loading : false});
        })
         .catch(error => {
             console.log(error);
             this.setState({loading : false});
         });
         this.props.history.replace('/');
    }
    checkValidation = (value,rules) => {
        let isValid = true;
        if(rules.isRequired){
            isValid = isValid && Boolean(value.trim());
        }
        if(rules.minLength){
            isValid = isValid && (value.length >= rules.minLength);
        }
        return isValid;
    }
    onInputChange = (event,type) => {
        const newForm = {...this.state.Orderform};
        const newFormElement = {...newForm[type]};
        newFormElement.value = event.target.value;
        newFormElement.touched = true;
        newFormElement.isValid = this.checkValidation(newFormElement.value,newFormElement.validation);
        newForm[type] = newFormElement;
        let formIsValid = true;
        Object.keys(newForm).map(el => {
            formIsValid = formIsValid && newForm[el].isValid;
        })
        this.setState({Orderform : newForm,formIsValid : formIsValid});
    }
    render(){
        let form = (
            <form onSubmit={this.onOrderClicked}>
                {Object.keys(this.state.Orderform).map(el =>{
                    const obj = this.state.Orderform[el];
                    return (
                        <Input {...obj} key={el} onChange={(event) => this.onInputChange(event,el)}/>
                    );
                })}
                <Button btnType="Success" disabled={!this.state.formIsValid}> Order </Button>
            </form>
        )
        if(this.state.loading){
            form = <Spinner />
        }
        return (
            <div className={styles.ContactData}>
            <h4>Enter your contact data!</h4>
            {form}
        </div>
        )
    };
};


const mapStateToProps = state => {
    return {
        ingredients : state.ingredients,
        price : state.totalPrice,
    };
};
export default connect(mapStateToProps)(withRouter(ContactData));