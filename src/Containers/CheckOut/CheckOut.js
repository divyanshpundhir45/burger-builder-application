import React , {Component} from 'react';
import { Route } from 'react-router-dom';
import CheckOutSummary from '../../Components/Order/CheckOutSummary/CheckOutSummary';
import ContactData from '../ContactData/ContactData';
import { connect } from 'react-redux';

class CheckOut extends Component {

    continueClicked = () => {
        this.props.history.push({
            pathname : this.props.match.url + '/contact-data',
        });
    }
    cancelClicked = () => this.props.history.goBack();

    render (){
        return (
            <div>
                <CheckOutSummary 
                  ingredients={this.props.ingredients} 
                  checkoutCancelled={this.cancelClicked}
                  checkoutContinued={this.continueClicked}
                />
                <Route 
                  path={`${this.props.match.url}/contact-data`}
                  exact
                  component={ContactData}
                />
            </div>
        );
    };
};

const mapStateToProps = state => {
    return {
        ingredients : state.ingredients,
        price : state.totalPrice,
    };
};

export default connect(mapStateToProps)(CheckOut);