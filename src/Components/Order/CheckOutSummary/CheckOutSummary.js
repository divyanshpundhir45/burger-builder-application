import React from 'react';
import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';
import styles from './CheckOutSummary.module.css';

const CheckOutSummary = (props) => {
    return (
        <div className={styles.CheckOutSummary}>
            <h1>We hope it tastes well!</h1>
            <div style={{margin : 'auto' , width : '100%'}}>
                <Burger ingredients={props.ingredients}/>
            </div>
            <Button btnType="Success" onClicked={props.checkoutCancelled}>CANCEL</Button>
            <Button btnType="Danger" onClicked={props.checkoutContinued}>CONTINUE</Button>
        </div>
    );
};

export default CheckOutSummary;
