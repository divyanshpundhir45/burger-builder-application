import React from 'react';

import styles from './Order.module.css';

const Order = (props) => {
    let price = props.price.toFixed(2);
    let transformedIngredients = [];
    for(let igkey in props.ingredients){
        transformedIngredients.push({name : igkey , amount : props.ingredients[igkey]});
    }
    console.log(transformedIngredients);
    const ingredientsOutput = transformedIngredients.map(ingredient => (
        <span 
          key={ingredient}
          style={{
              textTransform  : 'capitalize',
              display : 'inline-block',
              margin : '0 8px',
              border : '1px solid #ccc',
              padding : '5px',
            }}
          >
            {ingredient.name} ({ingredient.amount})
        </span>
    ))
    return (
        <div className={styles.Order}>
            <div className={styles.name}> Order by - <strong> {props.name} </strong></div>
            <p>Ingredients : {ingredientsOutput}</p>
            <p>Price : <strong>${price}</strong></p>
        </div>
    );
};

export default Order;
