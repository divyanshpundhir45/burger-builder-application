import React from 'react';
import styles from './DrawerToggle.module.css';

const DrawerToggle = (props) => (
    <div onClick={props.onClicked} className={styles.DrawerToggle}>Menu</div>
);

export default DrawerToggle;