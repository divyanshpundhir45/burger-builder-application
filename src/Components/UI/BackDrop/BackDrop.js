import React from 'react';

import styles from './BackDrop.module.css';

const BackDrop = (props) => {
   const { className = '' } = props;
   const arr = [styles.BackDrop,className];
   return (
    props.show ? <div className={arr.join(' ')} onClick={props.onClicked}></div> : null
   ); 
};


export default BackDrop;