import React from 'react';
import styles from './Modal.module.css';
import Aux from '../../../hoc/Auxiliary';
import BackDrop from '../BackDrop/BackDrop';

const Modal = (props) => (
    <Aux>
        <BackDrop show={props.show} onClicked={props.onModalCancel}/>
        <div 
         className={styles.Modal}
         style = {{
            transform : props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity : props.show ? '1' : '0',
         }}>
        {props.children}
        </div>
    </Aux>
);

export default Modal;