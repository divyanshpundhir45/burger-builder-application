import React from 'react';
import styles from './Input.module.css';

const Input = (props) => {
    let inputElement = null;
    const inputClasses = [styles.InputElement];
    if(!props.isValid && !props.shouldNotValidate && props.touched){
        inputClasses.push(styles.invalid);
    }
    switch(props.elementType){
        case('input') :
            inputElement = <input className={inputClasses.join(' ')} value={props.value} {...props.elementConfig} onChange={props.onChange}/>;
            break;
        case('textarea') :
            inputElement = <textarea className={inputClasses.join(' ')} value={props.value} {...props.elementConfig} onChange={props.onChange}/>
            break;
        case('select') : 
            inputElement = (
                <select className={inputClasses.join(' ')} value={props.value} onChange={props.onChange}>
                    {props.elementConfig.options.map(opt => (
                        <option value={opt.value} key={opt.value}>{opt.displayValue}</option>
                    ))}
                </select>
            )
            break;
        default :
            inputElement = <input className={inputClasses.join(' ')} value={props.value} {...props.elementConfig} onChange={props.onChange}/>;
            break;
            
    }
    return (
        <div className={styles.Input}>
            <label className={styles.Label}>{props.label}</label>
            {inputElement}
        </div>
    );
};

export default Input;