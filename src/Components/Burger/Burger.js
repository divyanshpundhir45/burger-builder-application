import React from 'react';

import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import styles from './Burger.module.css';


const Burger =(props) =>{

    const { ingredients = {} } = props;
    let transformedIngredients = [];
    Object.keys(ingredients).map(v =>{
        const t = ingredients[v];
        for(let i=0;i<t;i++)
        transformedIngredients.push(<BurgerIngredient key={v + i} type={v} />)
    });

    const getBurger = () =>{
        if(transformedIngredients.length === 0){
            transformedIngredients.push(<div key="div-key">Please start adding ingredients</div>);
        }
        transformedIngredients.push(<BurgerIngredient type="bread-bottom" key='first' />);
        transformedIngredients.unshift(<BurgerIngredient type="bread-top" key='last' />);
        return transformedIngredients;
    }
    return (
        <div className={styles.Burger}>
              {getBurger()}
        </div>
    );
};

export default Burger;