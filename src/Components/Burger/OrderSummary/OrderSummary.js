import React from 'react';
import Button from '../../UI/Button/Button';
import Aux from '../../../hoc/Auxiliary';


const OrderSummary = (props) => {
    const ingredientsList = Object.keys(props.ingredients)
                              .map(igName =>(
                                  <li key={igName}>
                                    <span style={{textTransform : 'capitalize'}}>{igName}</span> : {props.ingredients[igName]}
                                  </li>
                              ))
    return (
        <Aux>
            <h3>Your Order</h3>
            <p>A delicious burger with following ingredients : </p>
           <ul>
               {ingredientsList}
           </ul>
           <h3>Your total billing amount is  ${props.billingAmount.toFixed(2)} </h3>
           <p> Continue to place the order </p>
           <Button btnType="Danger" onClicked={props.purchaseCancel}> Cancel </Button>
           <Button btnType="Success" onClicked={props.purchaseSuccess}> Order </Button>
        </Aux>
    )

};

export default OrderSummary;