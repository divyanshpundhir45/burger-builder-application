import React from 'react'

import styles from './BuildControl.module.css';

const BuildControl = (props) => {
    return (
        <div className={styles.BuildControl}>
            <div className={styles.Label}>{props.label}</div>
            <button className={styles.More} onClick={() => props.onMore(props.type)}>
                 More
            </button>
            <button 
             className={styles.Less} 
             onClick={() => props.onLess(props.type)}
             disabled={props.isDisabled}
              >
                Less 
            </button>            
        </div>
    );
};

export default BuildControl;
