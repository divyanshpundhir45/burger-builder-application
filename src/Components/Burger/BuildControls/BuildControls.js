import React , {useState} from 'react'
import BuildControl from './BuildControl/BuildControl';
import styles from './BuildControls.module.css';


const controls = [
    {
        label : 'Salad',
        type : 'salad',
    },
    {
        label : 'Bacon',
        type : 'bacon',
    },
    {
        label : 'Meat',
        type : 'meat',
    },
    {
        label : 'Cheese',
        type : 'cheese',
    },
];

const BuildControls = (props) => {
    const {
        addIngredient,
        removeIngredient,
        disabledInfo,
        onOrder,
        orderButtonDisabled,
    } = props;
    return (
        <div className={styles.BuildControls}>
          <p>Current Price - ${props.price.toFixed(2)}</p>
          {controls.map(control => (
             <BuildControl 
              key={control.type} 
              label={control.label}
              type = {control.type}
              onMore={addIngredient}
              onLess={removeIngredient}
              isDisabled={disabledInfo[control.type]}
             />
          ))}  
          <button 
           className={styles.OrderButton}
           disabled={orderButtonDisabled}
           onClick={onOrder}
           >
               ORDER NOW
          </button>          
        </div>
    )
}

export default BuildControls;
