import React , {useState } from 'react';
import Aux from '../../hoc/Auxiliary';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';
import styles from './Layout.module.css';

const Layout = (props) => {

    const [showSideDrawer , setShowSideDrawer] = useState(false);

    const onToggleSideDrawer = () => setShowSideDrawer(!showSideDrawer);
    return (
        <Aux>
            <Toolbar onSideBarOpen={onToggleSideDrawer}/>
            <SideDrawer open={showSideDrawer} closed={onToggleSideDrawer}/>
            <main className={styles.Content}>
                {props.children}
            </main>
        </Aux>
    )
}

export default Layout;
