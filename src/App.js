import React, { Component } from "react";
import Layout from './Components/Layout/Layout';
import BurgerBuilder from './Containers/BurgerBuilder/BurgerBuilder';
import CheckOut from './Containers/CheckOut/CheckOut';
import {Switch, Route } from 'react-router-dom';
import Orders from "./Containers/Orders/Orders";

class App extends Component {
  render() {
    return (
        <Layout>
          <Switch>
             <Route path='/checkout' component={CheckOut} />
             <Route path='/orders' component={Orders} />
             <Route path='/' exact component={BurgerBuilder} />
          </Switch>
        </Layout>
    );
  };
};
export default App;
