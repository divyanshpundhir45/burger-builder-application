import axios from 'axios';

const instance = axios.create({
    baseURL : 'https://react-burger-builder-f34a6.firebaseio.com/',
});


export default instance;